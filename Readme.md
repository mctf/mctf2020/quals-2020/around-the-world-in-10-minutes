# Around the world in 10 minutes

Вам предложеная ставка в целый флаг участниками клуба mctf, вы должны вместе с 
вашим камердинером посетить 10 стран и вернуться обратно, совершив путешествие
вокруг света. Благодаря современным технологиям клуб будет следить за
вашим передвижением онлайн, вам всего надо отмечаться на сайте <...>.
В условиях пари у вас есть всего 1 минута на каждую страну, так что поспешите!
