from flask import Flask, Response, session, request, render_template_string
import time
import os

import geoip2.database
geoip = geoip2.database.Reader('/usr/share/GeoIP/GeoLite2-Country.mmdb')

import logging
logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
app.secret_key = os.urandom(16)

travel_list = [
    ('RU', 'Россия', 'Настоящие цтферы никогда не шутят, когда дело идет о столь серьезном деле, как пари. Мы поставили целый флаг против того, кто совершит путешествие вокруг света за 10 минут - 600 секунд или 600000 илисекунд!'),
    ('IN', 'Индия', """– Железная дорога кончилась, сударь!
– Но ведь газеты объявили, что дорога полностью открыта!
– Что делать, газеты ошиблись"""),
    ('SG', 'Сингапур', ''),
    ('JP', 'Япония', """- У вас все еще московское время, а оно отстаёт от здешнего приблизительно на шесть часов. Вам следует в каждой стране переводить часы на местное время.
- Мне! Переводить часы! Никогда!
- Но тогда они не будут соответствовать солнцу.
- Тем хуже для солнца! Значит, оно ошибается!"""),
    ('US', 'США', 'Поезд остановился в Нью-Йорке, и путешественники поспешили в порт, где должен был стоять на якоре пароход до Ливерпуля'),
    ('UK', 'Соединенное королевство', """Замечательное учреждение, именуемое английским банком, самым ревностным образом оберегает достоинство своих клиентов и поэтому не имеет ни охраны, ни даже решёток. Золото, серебро, банковские билеты открыто лежат повсюду и предоставлены, так сказать, "на милость" первого встречного. Разве допустимо подвергать сомнению честность своих посетителей?
    """),
    ('DE', 'Германия', ''),
    ('EG', 'Египет', 'Господа, это конец дороги, дальше её ещё не построили.'),
    ('TR', 'Турция', ''),
    ('RU', 'Россия', """Ещё одна минута – и пари будет выиграно. Все считали секунды. На сороковой секунде – ничего! На пятидесятой – всё ещё ничего! На пятьдесят пятой секунде с улицы донёсся шум, походивший на раскаты грома, послышались аплодисменты, крики «ура» и даже проклятия, – всё это слилось в общий несмолкаемый гул. Игроки поднялись со своих мест. На пятьдесят седьмой секунде дверь салона отворилась, и маятник часов не успел ещё качнуться в шестидесятый раз, как на пороге показался %username% в сопровождении обезумевшей толпы, которая насильно ворвалась за ним в клуб.
– Вот и я, господа, – произнёс он спокойным голосом.""")
]

FLAG = os.getenv('FLAG')

template = '''
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>travel progress</title>
<style>
@import url("https://fonts.googleapis.com/css?family=Roboto:400,400i,700");
body {
  background-color: #1d1d1d;
  min-height: 100vh;
  margin: 0;
  font-family: "Roboto", sans-serif;
  color: #fff;
  margin-bottom: 60px;
}
body .entries .entry:before {
  content: "";
  position: absolute;
  top: -15px;
  left: calc(100% + 45px);
  bottom: 15px;
  -webkit-transform: translateX(-50%);
          transform: translateX(-50%);
  width: 4px;
  background-color: #fff;
}
body .entries .entry:nth-child(2n):before {
  left: -35px;
}
body .entries {
  width: calc(100% - 80px);
  max-width: 800px;
  margin: auto;
  position: relative;
  left: -5px;
  margin-bottom: 90px;
}
body .entries .entry {
  width: calc(50% - 80px);
  padding: 20px;
  clear: both;
  text-align: right;
  position: relative;
}
body .entries .entry:not(:first-child) {
  margin-top: -60px;
}
body .entries .entry .title {
  font-size: 32px;
  margin-bottom: 12px;
  position: relative;
}
body .entries .entry .title:before {
  content: "";
  position: absolute;
  width: 8px;
  height: 8px;
  border: 4px solid #ffffff;
  background-color: #1d1d1d;
  border-radius: 100%;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  right: -73px;
  z-index: 1000;
}
body .entries .entry .title.big:before {
  width: 24px;
  height: 24px;
  -webkit-transform: translate(8px, -50%);
          transform: translate(8px, -50%);
}
body .entries .entry:last-child:before {
    display: none;
}
body .entries .entry .body {
  color: #aaa;
}
body .entries .entry .body p {
  line-height: 1.4em;
}
body .entries .entry:nth-child(2n) {
  text-align: left;
  float: right;
}
body .entries .entry:nth-child(2n) .title:before {
  left: -63px;
}
body .entries .entry:nth-child(2n) .title.big:before {
  -webkit-transform: translate(-8px, -50%);
          transform: translate(-8px, -50%);
}
body .entries .entry.green {
  color: #8bc34a;
}
body .entries .entry.green:before {
  background: #8bc34a;
}
body .entries .entry.green .title:before {
  border-color: #8bc34a;
}
button { padding: 10px 15px; border: 1px solid #fff; background: none;
  color: #fff; border-radius: 1px; margin-top: 30px; cursor: pointer;
  transition: color .3s, background .3s; text-transform: uppercase; font-weight: bold; }
button:hover { background: #ddd; color: #333; }

.tc { text-align: center; }
.g { display: flex; justify-content: center; align-items: center; margin-left: -30px; }
.g > * { margin-left: 30px; }

.sp1 { margin-top: 45px; }
</style>
</head>
<body>

<div class="entries">
{% for i, (_, name, flag_chunk) in travel_list %}
    <div class="entry {{ 'green' if i < session['travel_progress'] }}">
        <div class="title {{ 'big' if  i % 3 == 0 or i % 5 == 0 }}">{{ name }}</div>
        <div class="body">
            {% if i < session['travel_progress'] %}
                {{ flag_chunk }}
            {% endif %}
            <br><br><br>
        </div>
    </div>
{% endfor %}
</div>

<div class="g">
    <h2 class="tc">Оставшееся время <br> <span id="time"></span> </h2>
    <h2 class="tc"> Ваша Геолокация <br> {{ ip }} ({{ country_iso }}) </h2>
</div>

<div class="g">
  <button onclick="location.reload()">Проверить геолокацию</button>
</div>

{% if session['show_flag'] %}
  <h1 class="tc sp1">{{ flag }}</h1>
{% endif %}

<script>

var gtime = {{ session['time'] }};
function timer() {
    var time = Date.now() / 1000;
    var lasts = Math.floor(600 - time + gtime);
    document.getElementById('time').innerText = Math.floor(lasts / 60) + ':' + String(lasts % 60).padStart(2, '0');
    if (lasts <= 0) {
        location.reload();
        return;
    }
    setTimeout(timer, 1000);
}

timer()

</script>
</body>
</html>
'''

@app.route('/')
def index():
    logging.info(request.headers)
    ip = request.remote_addr
    try:
        response = geoip.country(ip)
        country_iso = response.country.iso_code if response.country.iso_code else "Unknown"
    except:
        country_iso = "Unknown"
        
    show_flag = session.get('show_flag')
    
    if 'time' not in session or (time.time() - session['time'] > 600 and not show_flag):
        session['time'] = time.time()
        session['travel_progress'] = -1
        logging.info('Сессия не найдена. создаем новую')
    else:
        if not show_flag:
            country = travel_list[session['travel_progress']][0]
            if country_iso == country:
                session['travel_progress'] += 1
                logging.info('Этап++: {}'.format(session['travel_progress']))
                
            if session['travel_progress'] >= len(travel_list):
                session['show_flag'] = True
                logging.info('Флаг получен')
        
    return render_template_string(template,
        session=session,
        travel_list=enumerate(travel_list),
        ip=ip,
        country_iso=country_iso,
        flag=FLAG)

try:
    app.run(host="0.0.0.0")
except KeyboardInterrupt:
    pass
except Exception as err:
    logging.exception(err)
